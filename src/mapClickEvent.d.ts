export type MapClickEvent = {
  nodes: Array<string>,
  edges: Array<string>,
  event: unknown,
  pointer: {
    DOM: {x:number, y:number},
    canvas: {x:number, y:number}
  }
}