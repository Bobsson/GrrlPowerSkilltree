/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
import { Network } from 'https://cdn.skypack.dev/vis-network/esnext?dts';
import type { MapClickEvent } from './mapClickEvent.d.ts'; 
/* top level orb map module mostly passes stuff to sub modules */

// the module that makes the map
import {makeMap} from './orbMap/makeMap.ts';

// the module that handles the click events on the map
import {mapClick} from './orbMap/onNodeClick.ts';

// the module that handles search events
import {addSearch} from './orbMap/findNode.ts';

/* end imports */

// make network global so it can be passed to stuff when needed. (can this be moved into `onLoadScript`?)
var network: null | Network = null;

// make the map.
network = makeMap();

// Whenever a node is clicked on, display it's data.
network.on( 'click', function(properties: MapClickEvent) {
  // just pass it to the mapClick function
  mapClick(properties);
});
// End of click event

// run the `addSearch` function.
addSearch(network);