import { Edge } from "./mapDataset/mapClasses/EdgeClass.ts";

//These two lines are a hack to export the stupid type data.
import type { Orb as OrbType } from "./mapDataset/mapClasses/OrbClass.ts";
export type Orb = OrbType;

// This file gethers all the files and passes them out as a single item.

import {comOrbNodes, comOrbEdges} from "./mapDataset/communications.ts";
import {lightHookNodes, lightHookEdges} from "./mapDataset/lightHook.ts";
import {PPONodes, PPOEdges} from "./mapDataset/weapons.ts";
import {FLTNodes, FLTEdges} from "./mapDataset/propulsion.ts";
import {MBNodes, MBEdges} from "./mapDataset/shields.ts";
import {UKNodes, UKEdges} from "./mapDataset/unknown.ts";
import {LSNodes, LSEdges} from "./mapDataset/life_support.ts";
import {outerCenterNodes, outerCenterEdges} from "./mapDataset/outerCenter.ts";
import {innerCenterNodes, innerCenterEdges} from "./mapDataset/innerCenter.ts";
import {nodeGroupStyles} from "./orbStyles.ts";


var orbNodes: Array<Orb> = [...comOrbNodes, ...lightHookNodes, ...PPONodes, ...FLTNodes, ...MBNodes, ...UKNodes, ...LSNodes];
var orbEdges: Array<Edge> = [...comOrbEdges, ...lightHookEdges, ...PPOEdges, ...FLTEdges, ...MBEdges, ...UKEdges, ...LSEdges];


// this combines all the lists into a single list for the map to use.
// this is how we maintain easy to read code!
var allNodes: Array<Orb> = [...orbNodes, ...outerCenterNodes, ...innerCenterNodes];
var allEdges: Array<Edge> = [...orbEdges, ...outerCenterEdges, ...innerCenterEdges];

export var orbDataset = {nodes: allNodes, edges: allEdges, nodeStyles: nodeGroupStyles};

// Ok, fine, I lied, there are two outputs.
import {absoluteDefaultMetadata, allGroupDefaultMetadata, generalSettings} from "./metadata_and_settings.ts";
export type { OrbMetadata } from "./metadata_and_settings.ts";
export var options = {'defaultMD': absoluteDefaultMetadata, 'groupMD':allGroupDefaultMetadata, 'settings':generalSettings}