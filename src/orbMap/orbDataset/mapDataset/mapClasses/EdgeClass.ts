export type EdgeOptions = {
  dashed?: boolean;
  arrow?: boolean;
  straight?: boolean;
  length?: number;
}

export class Edge {

  readonly id: string;
  //readonly id: undefined;
  readonly from: string;
  readonly to: string;
  width: number;
  color: {color?: string, highlight?: string, inherit?: false, opacity?:number} | string;
  hidden: boolean;
  dashes: number[] | false;
  arrows?: "to";
  smooth?: false;
  length?: number;

  constructor(from: string, to: string, status?: "En" | "Dis" | "Invs", edgeOptions?: EdgeOptions) {
    // set basic stuff
    this.from = from.toUpperCase();
    this.to = to.toUpperCase();
    this.id = `${this.from}-e>${this.to}`;
    //this.id = undefined;
    this.width = 4
    this.hidden = false;
    this.dashes = false;

    if(status == "En"){
      this.color = {color:'#0125FF', highlight:'#0125FF', inherit:false};
    } else if(status == "Dis"){
      this.color = {color:'#3CC2E0', highlight:'#3CC2E0', inherit:false};
    } else if(status == "Invs"){
      this.color = {color:'silver', highlight:'silver', inherit:false};
      this.hidden = true;
    } else {
      this.color = {color:'red', highlight:'orange', inherit:false};
    }

    if(edgeOptions?.dashed){
      // send Rainbow
      this.dashes = [24,8,8,8];
    }

    if(edgeOptions?.arrow){
      this.arrows = 'to';
    }

    if(edgeOptions?.straight){
      this.smooth = false;
    }

    if(edgeOptions?.length){
      if(edgeOptions.length != -1){
        this.length = edgeOptions.length;
      }
    } else {
      this.length = 30;
    }
  }
}

export function edge(from: string, to: string, status?: "En" | "Dis" | "Invs", edgeOptions?: EdgeOptions) {
  return (new Edge(from, to, status, edgeOptions));
}
