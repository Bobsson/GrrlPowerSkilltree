import { nodeGroupStyles } from "./../../orbStyles.ts";

export type udn = undefined | null;

export type JSONText = string | {
  k?: "t" | "ul" | "a" | "sup",
  c?: Array<JSONText> | JSONText
  p?: string
  l?: string,
  t?: string,
}

export interface nodeMetaData {
  name?: string,
  status?: string,
  notes?: JSONText,
  nodeNote?: true,
  unlocked?: JSONText,
  nodeUnlockedBasic?: true,
  ull?: unknown,
  nodeUnlockedLink?: true,
  whatItDoes?: JSONText,
  nodeWhatItDoes?: true,
  beautifulID?: string,
}

export class Orb {
  readonly id: string;
  readonly group: string;
  readonly label?: string;
  readonly image?: string;
  x?: number;
  y?: number;
  fixed?: Record<string, boolean>;
  color?: {border: string, background: string};
  borderWidth?: number;
  size?: number;
  data: nodeMetaData;

  constructor(id: string, group: string, x?: number | "zero" , y?: number | "zero" , label?: string, image?: string, data?: nodeMetaData, commentBorder?: "On" | "off") {
    // set basic stuff
    this.id = id.toUpperCase();
    this.group = group;

    if(label) {
      this.label = label;
    }
    if(image){
      this.image = image;
    }

    const tempFixed: Record<string, boolean> = {}

    // Set X & Y
    if(x){
      if(x == 'zero'){
        this.x = 0;
      } else {
        this.x = -x;
      }
      tempFixed.x = true;
    }
    if(y){
      if(y == 'zero'){
        this.y = 0;
      } else {
        this.y = y;
      }
      tempFixed.y = true;
    }
    // if X or Y, fix the point
    if((x)||(y)){
      this.fixed = tempFixed;
    }

    // data is also used if the id is not all caps, have to trigger in both sanarios.
    if ( (data) || (this.id !== id)){
      const tempData = data ?? {};
      if (tempData.notes){
        tempData.nodeNote = true;
      }
      if (tempData.unlocked){
        tempData.nodeUnlockedBasic = true;
      }
      if (tempData.ull){
        tempData.nodeUnlockedLink = true;
      }
      if (tempData.whatItDoes){
        tempData.nodeWhatItDoes = true;
      }
      if (this.id !== id){
        tempData.beautifulID = id;
      }
      this.colorModder(group, commentBorder, tempData);
      this.data = tempData ?? {};
    } else {
      this.data = {};
    }
  }

  colorModder(group: string, commentBorder: "On" | "off" | udn, data: nodeMetaData) {
      var bg = 'white';
      var bdr = 'white';
      var comment = 'gold';

      bg = nodeGroupStyles[group].color?.background ?? bg;
      bdr = nodeGroupStyles[group].color?.border ?? bdr;

      var changed = false;
      var borderWidth = false;

      if(data.nodeUnlockedLink){
        bg = '#00CC84';
        comment = 'orange';
        changed = true;
      }

      if(data.nodeNote){
          if(commentBorder){
            if(commentBorder == "On"){
              bdr = comment;
              changed = true;
              borderWidth = true;
            } else {
              changed = false;
              borderWidth = false;
            }
          } else {
            const skipThese = ["Hidden", "Orb", "TrueCenter"]
            if(skipThese.indexOf(group) == -1){
              bdr = comment;
              changed = true;
              borderWidth = true;
            }
          }
      }
      if(changed){
        this.color = { border : bdr, background : bg};
      } else {
        delete this.color;
      }
      if(borderWidth){
        this.borderWidth = 3;
      } else {
        delete this.borderWidth;
        delete this.size;
      }
  }
}

export function orb(id: string, group: string, x?: number | "zero", y?: number | "zero", label?: string, image?: string, data?: nodeMetaData, commentBorder?: "On" | "off") {
  return (new Orb(id, group, x, y, label, image, data, commentBorder));
}
