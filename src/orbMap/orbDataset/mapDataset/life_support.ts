import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";


// Life Support Orb
export const LSNodes = [
    orb( 'LS', 'Orb', 300, 580, 'Life Support Orb', './images/Orbs/LifeSupport_Orb.png', {
      status:"Pine Fresh",
      notes:"Can it be used in a vacuum?",
      whatItDoes:{
        k:"ul",
        c:[
          "Created air underwater, can provide breathable air from water and air(that has insufficent O2 content)."
        ]
      }
    }),
    orb( 'LS.A.1', 'Purchased', 520, 540, undefined, undefined, {
      notes:{
        k:"ul",
        c:[
          "Original: Could this allow it to create air in a vacuum?",
          "Possibly a passive ability.",
          {c:["Immunity to poisons? (Why she can eat the unmaker?)",
          {
            k: "sup",
            c: {
              k: "a",
              p: "118"
            }
          }]}
        ]
    }
    }),
    orb( 'LS.B.1', 'Purchased', 460, 660, undefined, undefined, {
      notes:"Sents? Possibly range of effect."
    }),
    orb( 'LS.B.2', 'Available'),
    orb( 'LS.B.3', 'Available'),
    orb( 'LS.C.1', 'Available', 200, 760),
    orb( 'LS.C.2', 'Available'),
    orb( 'LS.C.3', 'Available'),
    orb( 'LS.C.4', 'Available', 500, 860)
];

export const LSEdges = [
    edge('LS', 'LS.A.1', "En", {dashed: true}),
    edge('LS', 'LS.B.1', "En"),
    edge('LS.B.1', 'LS.B.2', "En"),
    edge('LS.B.2', 'LS.B.3', "En"),
    edge('LS', 'LS.C.1', "En"),
    edge('LS.C.1', 'LS.C.2', "En"),
    edge('LS.C.2', 'LS.C.3', "En"),
    edge('LS.C.3', 'LS.C.4', "En"),
];
