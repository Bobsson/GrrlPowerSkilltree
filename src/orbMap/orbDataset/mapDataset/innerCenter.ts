import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";


//Inner Center Orbs
export const innerCenterNodes = [
    orb( 'Center.True', 'TrueCenter', 'zero', 'zero', undefined, undefined, {
      notes:{
        k:"ul",
        c:[
          "most likely will become fully enabled if all connected nodes were enabled.",
          "May enable viewing past the Vanishing Point upgrades.",
          "Possibly unlocks a new orb",
          "Image should have 3 wedges purchased, Will update eventually."
        ]
      },
      unlocked: {
        k:'ul',
        c:[
          {
            c:[
              "Wedge 1",
              "Prior to obtaining orbs"
            ]
          },
          {
            c:[
              "Wedge 2",
              "Prior to obtaining orbs"
            ]
          },
          {
            c:[
              "Wedge 3",
              {
                k:'a',
                p:'672'
              }
            ]
          }
        ]
      },
      name:"True Center"
    }),
    orb( 'Center.CO', 'Off', 210, 45, undefined, undefined, {
      notes:"Most likely connected in some way to the Communications Orb"
    }, "off"),
    orb( 'Center.LH', 'Off', 165, -150, undefined, undefined, {
      notes:"Most likely connected in some way to the Light Hook Orb"
    }, "off"),
    orb( 'Center.PPO', 'Off', 'zero', -240, undefined, undefined, {
      notes:"Most likely connected in some way to the Weapons Orb"
    }, "off"),
    orb( 'Center.FLT', 'On', -165, -150, undefined, undefined, {
      notes:"Most likely connected in some way to the Propulsion Orb"
    }, "off"),
    orb( 'Center.MB', 'On', -210, 45, undefined, undefined, {
      notes:"Most likely connected in some way to the Shield Orb"
    }, "off"),
    orb( 'Center.UK', 'On', -105, 180, undefined, undefined, {
      ull: true,
      unlocked:{
        k:"a",
        p:"672"
      },
      notes:{
        k:"ul",
        c:[
          "Most likely connected in some way to the 'Still Unknown' Orb",
          {c:["It was originally unclear if this or Center.PPO was purchased. Later clarified in", {k:"a",p:"944"}, "."]}
        ]
      }
    }, "off"),
    orb( 'Center.LS', 'Off', 105, 180, undefined, undefined, {
      notes:"Most likely connected in some way to the Life Support Orb"
    }, "off")
];

const centerEdgesOptions = {straight: true, length:-1}
const centerEdgesOptions2 = {dashed: true, length:-1}

const innerCenterHidden = [
    edge('LH', 'Center.LH', 'Invs', centerEdgesOptions),
    edge('CO', 'Center.CO', 'Invs', centerEdgesOptions),
    edge('PPO', 'Center.PPO', 'Invs', centerEdgesOptions),
    edge('FLT', 'Center.FLT', 'Invs', centerEdgesOptions),
    edge('MB', 'Center.MB', 'Invs', centerEdgesOptions),
    edge('UK', 'Center.UK', 'Invs', centerEdgesOptions),
    edge('LS', 'Center.LS', 'Invs', centerEdgesOptions),
];
const innerCenterVisible = [
    edge('Center.LH', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.CO', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.PPO', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.FLT', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.MB', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.UK', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.LS', 'Center.True', 'En', centerEdgesOptions),
    edge('Center.LH', 'Center.CO', 'Dis', centerEdgesOptions2),
    edge('Center.PPO', 'Center.LH', 'Dis', centerEdgesOptions2),
    edge('Center.FLT', 'Center.PPO', 'Dis', centerEdgesOptions2),
    edge('Center.MB', 'Center.FLT', 'Dis', centerEdgesOptions2),
    edge('Center.UK', 'Center.MB', 'Dis', centerEdgesOptions2),
    edge('Center.LS', 'Center.UK', 'Dis', centerEdgesOptions2),
    edge('Center.CO', 'Center.LS', 'Dis', centerEdgesOptions2),
  ];

// this combines the two lists into one.
export const innerCenterEdges = [...innerCenterHidden, ...innerCenterVisible];
