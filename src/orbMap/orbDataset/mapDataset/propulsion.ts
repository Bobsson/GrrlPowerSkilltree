import {orb} from "./mapClasses/OrbClass.ts";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be undefined except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.ts";


// Flight Orb
export var FLTNodes = [
    orb( 'FLT', 'Orb', -520, -420, 'Transportation Orb', './images/Orbs/PropulsionOrb.png', {
      status:"The Flight / Transportation Orb",
      notes:"Why does the Com-Orb have a teleporter if this one provides Propulsion?<br />Does it provide a minor enviromental shield?",
      whatItDoes:{
        k:"ul",
        c:[
          "Flight up to Mach 16.",
          {c:[
            "Removes fear of heights <del>during use<del> always",
            {
              k: "sup",
              c: {
                k: "a",
                p: "946"
              }
          }]},
          "Generates 'Aetherium Causeways' (Wormholes)."
        ]
      }
    }),
    orb( 'FLT.A.1', 'Purchased', -700, -240, "Featherfall", undefined, {
      ull: true,
      notes:"Teleporter unlock looked similar to this one.<br />Encapsulated nodes are passive abilities?",
      unlocked:{
        k:"a",
        p:"945"
      },
      whatItDoes:{
        k:"ul",
        c:[
          {c:["Enables featherfall / slow fall",
          {
            k: "sup",
            c: {
              k: "a",
              p: "946"
            }
          }]},
          {c:["Doesn't need for the orb to be held to be activated",
          {
            k: "sup",
            c: {
              k: "a",
              p: "946"
            }
          }]}
        ]
      }
    }),
    orb( 'FLT.B.1', 'Purchased', -780, -320, 'Flight', undefined, {
      notes:"This is the speed upgrade tree.",
      whatItDoes:{
        k:"ul",
        c:[
          "Base speed should be 12 Mph or 19 Km/h (5.4 m/s)",
          "This one should provide speed up to 47 Mph or 77 Km/h (21.4 m/s)"
        ]
      }
    }),
    orb( 'FLT.B.1.A.1', 'Purchased', -900, -320, 'Faster', undefined, {
      whatItDoes:"Should provide speed up to 191 Mph or 308 Km/h (85.75 m/s)"
    }),
    orb( 'FLT.B.1.A.2', 'Purchased', undefined, undefined, 'Faster', undefined, {
      whatItDoes:"Should provide speed up to Mach 1. (343 m/s)"
    }),
    orb( 'FLT.B.1.A.3', 'Purchased', undefined, undefined, 'Faster', undefined, {
      whatItDoes:{
        k:"t",
        c:[
          "Provides speed up to Mach 4. (1372 m/s)",
          {
            k: "sup",
            c: {
              k: "a",
              p: "670"
            }
          }
        ]
      }
    }),
    orb( 'FLT.B.1.A.4', 'Purchased', -1180, -560, 'Faster', undefined, {
      ull: true,
      unlocked: {
        k:"a",
        p:"660"
      },
      whatItDoes:{
        k:"t",
        c:[
          "Provides speed up to Mach 16. (5488 m/s)",
          {
            k: "sup",
            c: {
              k: "a",
              p: "670"
            }
          }
        ]
      }
    }),
    orb( 'FLT.B.1.B.1', 'Purchased', -1120, -292, `Wormhole`, undefined, {
      ull: true,
      whatItDoes: {
        k:'ul',
        c:[
          {
            c:[
              "Generates a 'Aetherium Causeway' (Wormhole)",
              "<br />",
              {
                k:'ul',
                c:[
                  "Once a causeway closes no known traces are left.",
                  "'Conventional' technology to generate a Aetherium Causeway can only be mounted on capitol ships."
                ]
              }
            ]
          },
          {
            c: [
              "Avalible to purchase as of",
              {
                k:'a',
                p:'660'
              }
            ]
          }
        ]
      },
      unlocked:{
        k:"a",
        p:"672"
      },
      notes: "Unknown what the minimum & maximum ranges are if any."
    }),
    orb( 'FLT.B.1.B.2', 'Available', undefined, undefined, undefined, undefined, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.3', 'Available', undefined, undefined, undefined, undefined, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.4', 'Available', undefined, undefined, undefined, undefined, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.5', 'Available', -1360, -540, undefined, undefined, {
      notes:{
        c:[
          "Appears to be unlocked as of",
          {
            k:'a',
            p:'660'
          }
        ]
      }
    }, "off"),
    orb( 'FLT.B.1.B.5.VP', 'VanishingPoint', -1520, -480),
    orb( 'FLT.C.1', 'Purchased', -740, -360, undefined, undefined, {
      notes:"Turning radius? Inirtial Dampaners? How fast to go from 0 to Mach 6?"
    }),
    orb( 'FLT.C.2', 'Purchased'),
    orb( 'FLT.C.3', 'Available'),
    orb( 'FLT.C.4', 'Available'),
    orb( 'FLT.C.5', 'Available', -1020, -600),
    orb( 'FLT.D.1', 'Available', -760, -520),
    orb( 'FLT.D.2', 'Available'),
    orb( 'FLT.D.3', 'Available'),
    orb( 'FLT.D.4', 'Available'),
    orb( 'FLT.D.4.VP', 'VanishingPoint', -760, -820),
    orb( 'FLT.E.VP', 'VanishingPoint', -620, -680)
];


export var FLTEdges = [
    edge('FLT', 'FLT.A.1', "En", {dashed: true}),
    edge('FLT', 'FLT.B.1', "En"),
    edge('FLT.B.1', 'FLT.B.1.A.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.A.2', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.A.3', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.A.4', "En"),
    edge('FLT.B.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.4', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.B.1', 'FLT.B.1.B.2', "En"),
    edge('FLT.B.1.B.2', 'FLT.B.1.B.3', "En"),
    edge('FLT.B.1.B.3', 'FLT.B.1.B.4', "En"),
    edge('FLT.B.1.B.4', 'FLT.B.1.B.5', "En", {dashed: true}),
    edge('FLT.B.1.B.5', 'FLT.B.1.B.5.VP', "Dis", {arrow: true}),
    edge('FLT', 'FLT.C.1', "En"),
    edge('FLT.C.1', 'FLT.C.2', "En"),
    edge('FLT.C.2', 'FLT.C.3', "En"),
    edge('FLT.C.3', 'FLT.C.4', "En"),
    edge('FLT.C.4', 'FLT.C.5', "En"),
    edge('FLT', 'FLT.D.1', "En", {dashed: true}),
    edge('FLT.D.1', 'FLT.D.2', "En"),
    edge('FLT.D.2', 'FLT.D.3', "En"),
    edge('FLT.D.3', 'FLT.D.4', "En"),
    edge('FLT.D.4', 'FLT.D.4.VP', "Dis", {arrow: true}),
    edge('FLT', 'FLT.E.VP', "Dis", {arrow: true}),
];
