/// <reference lib="esNext" />
/// <reference lib="dom" />
/// <reference lib="webworker.importScripts" />
/// <reference lib="ScriptHost" />
/// <reference lib="dom.iterable" />
/// <reference no-default-lib="true"/>
import { Network } from 'https://cdn.skypack.dev/vis-network/esnext?dts';

/* <<< TOOLKIT SETUP >>> */
// import the toolkit
import {aggregator, toolkit} from "./../toolkit/baseToolkit.js";
import {eventToolkitExtention} from "./../toolkit/eventToolkitExtention.js";
import {formToolkitExtention} from "./../toolkit/formToolkitExtention.js";
import {urlToolkitExtention} from "./../toolkit/urlToolkitExtention.js";

class tk extends aggregator(toolkit, eventToolkitExtention, formToolkitExtention, urlToolkitExtention) {
  //deno-lint-ignore no-explicit-any
  constructor(...params: any){
    super(params);
  }

  addListener(type: string, f: (event: Event)=>void){
    super.addListener(type, f);
  }

  getUrlParam(s:string){
    return <string>super.getUrlParam(s);
  }
  value(v?: string){
    return <string>super.value(v);
  }
  setValididy(v: string){
    super.setValididy(v);
  }
  tellInvalid(){
    super.tellInvalid();
  }
}
/* <<< TOOLKIT SETUP DONE >>> */

// mapClick and findNode functions
import {mapClick, findNode} from './onNodeClick.ts';

/* end imports */

// set network as a global function.
var network: Network | null = null;

/* start `addSearch` function */
export function addSearch(passedNetwork: Network) {
  // make the global the network function
  network = passedNetwork;

  // check the url to see if there is a node in it
  __checkURL();

  // set up the handle search event
  new tk('#searchForm').addListener("submit", __onSearch);
  // end of search event

  // on search text change
  new tk('#NIDS').addListener("change", __onTextChange);
  // end of search text change
}
/* end `addSearch` function */


/* start `__checkURL` function */
function __checkURL() {
  if(!network){
    return;
  }

  // grab the URL node.
  var nodeToFind = new tk().getUrlParam('node');

  // if there is a node there
  if (nodeToFind !== undefined) {
    // TODO: make this into a function to reduce redundancy
    // make it upper case
    nodeToFind = nodeToFind.toUpperCase();

    // attempt to find the node in the dataset
    if (findNode(nodeToFind) !== undefined){
      // if it was found, run in a try-catch just in case
      try {
        // select the node on the map, but not the connected edges.
        network.selectNodes([nodeToFind], false);
        // set the search bar to the node ID
        new tk('#NIDS').value(nodeToFind);
        // run the map-click function to display the node's data.
        mapClick({nodes:[nodeToFind], edges:[], event: {}, pointer:{DOM: {x:0, y:0}, canvas: {x:0, y:0}}});

      } catch {
        // sometimes throws error when it shouldn't, so we don't warn for bad URL's
      }
    } else {
      // if we were not looking for a empty string
      if(nodeToFind !== ""){
        // warn about a invalid node.
        new tk('#NIDS').setValididy("Node Not Found.")
        new tk('#NIDS').tellInvalid();
      }
    }
  }
}
/* end `__checkURL` function */


/* start `__onSearch` function */
function __onSearch(event: Event) {
  if(!network){
    return;
  }
  // prevent actually submitting the search event.
  event.preventDefault();

  // get the node's id
  var nodeToFind = new tk('#NIDS').value().toUpperCase();

  //see above for most comments
  if (nodeToFind !== undefined) {
    nodeToFind = nodeToFind.toUpperCase();
    if (findNode(nodeToFind) !== undefined){
      try {
        network.selectNodes([nodeToFind], false);
        mapClick({nodes:[nodeToFind], edges:[], event: {}, pointer:{DOM: {x:0, y:0}, canvas: {x:0, y:0}}});
        new tk('#NIDS').value(nodeToFind);
      } catch {
        // if we have a error then it was probably the select nodes part,
        // display the node not found error.
        new tk('#NIDS').setValididy("Node Not Found.")
        new tk('#NIDS').tellInvalid();
      }
    } else {
      if(nodeToFind !== ""){
        new tk('#NIDS').setValididy("Node Not Found.")
        new tk('#NIDS').tellInvalid();
      }
    }
  }
}
/* end `__onSearch` function */



// Resets the valididy notification if the search text is changed.
function __onTextChange() {
  new tk('#NIDS').setValididy("");
}
