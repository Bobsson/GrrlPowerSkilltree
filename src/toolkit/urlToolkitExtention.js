/* esversion: 6 */

/**
 * Extention for working with the URL.
 * @extends {toolkit}
 */
export class urlToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * @public
   * @ignore
   */
  initializer(){
    this.addExtention("urlToolkitExtention");
  }

  /**
   * get's a spicific URL paramater.
   * @param {String} param - Paramater to get
   * @returns {String} value of paramater
   */
  getUrlParam(param) {
    return this.getUrlParams()[param];
  }

  /**
   * Get's all URL paramaters.
   * @returns {Object<String, String>} values of the paramaters.
   */
  getUrlParams() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var params = {};
    for(var pair of url.searchParams.entries()) {
       params[pair[0]] = pair[1];
    }
    this.params = params;
    return params;
  }

  /**
   * sets's URL paramaters.
   * @param {*} params - Paramaters
   * @todo figure out how this is supposed to be used.
   */
  setParams(params) {
    var fileName = (window.location.href.split("/").slice(-1)) + "";
    fileName = fileName.split('?')[0]
    this.updatePageURL(fileName + "?" + params);
  }

  /**
   * updates URL to the URL specified
   * @param {String} url - new URL to replace the current one with.
   */
  updatePageURL(url) {
    window.history.replaceState(null, null, url);
  }

};
